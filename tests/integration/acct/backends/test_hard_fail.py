import tempfile

import pytest
import yaml


@pytest.mark.asyncio
async def test_profiles(hub):
    data = {"acct-backends": {"bad": {"profile": None}}}
    with tempfile.NamedTemporaryFile(delete=True, mode="w") as fh:
        yaml.safe_dump(data, fh)
        fh.flush()

        # Make sure it passes without hard_fail
        await hub.acct.init.profiles(acct_file=fh.name)

        # Now it should blow up if hard_fail is True
        with pytest.raises(Exception):
            await hub.acct.init.profiles(acct_file=fh.name, hard_fail=True)


@pytest.mark.asyncio
async def test_unlock(hub):
    data = {"acct-backends": {"bad": {"profile": None}}}
    with tempfile.NamedTemporaryFile(delete=True, mode="w") as fh:
        yaml.safe_dump(data, fh)
        fh.flush()

        # Make sure it passes without hard_fail
        await hub.acct.init.unlock(acct_file=fh.name)
        # Allow the function to run again
        hub.acct.UNLOCKED = False

        # Now it should blow up if hard_fail is True
        with pytest.raises(Exception):
            await hub.acct.init.unlock(acct_file=fh.name, hard_fail=True)


@pytest.mark.asyncio
async def test_gather(hub):
    profiles = {
        "acct-backends": {"bad": {"profile": None}},
    }
    # Make sure the function call runs without any exceptions
    await hub.acct.init.gather(subs=["bad"], profile="profile", profiles=profiles)

    # Now the exact same function should blow up if hard_fail is True
    with pytest.raises(Exception):
        await hub.acct.init.gather(
            subs=["bad"], profile="profile", profiles=profiles, hard_fail=True
        )


@pytest.mark.asyncio
async def test_process(hub):
    profiles = {"provider.bad": {"profile": None}}
    # Make sure the function call runs without any exceptions
    await hub.acct.init.process(subs=["provider"], profiles=profiles)

    # Now the exact same function should blow up if hard_fail is True
    with pytest.raises(Exception):
        await hub.acct.init.process(
            subs=["provider"], profiles=profiles, hard_fail=True
        )


@pytest.mark.asyncio
async def test_backend_unlock(hub):
    data = {"acct-backends": {"bad": {"profile": None}}}

    # Make sure it passes without hard_fail
    await hub.acct.backend.init.unlock(profiles=data)

    # Now it should blow up if hard_fail is True
    with pytest.raises(Exception):
        await hub.acct.backend.init.unlock(profiles=data, hard_fail=True)


@pytest.mark.asyncio
async def test_metadata_gather(hub):
    profile = {"bad": {"profile": None}}

    # Make sure it passes without hard_fail
    await hub.acct.metadata.init.gather(profile=profile, providers=["bad"])

    # Now it should blow up if hard_fail is True
    with pytest.raises(Exception):
        await hub.acct.metadata.init.gather(
            profile=profile, providers=["bad"], hard_fail=True
        )
