import pytest

PROFILES = {
    "backend_key": "my_backend_key",
    "default": "my_default",
    "my_backend_key": {"provider": {"backend_profile": {"kwarg1": "val1"}}},
}


def test_tpath(hub):
    assert "provider" in hub.acct.backend
    assert hub.acct.backend.provider.unlock


@pytest.mark.asyncio
async def test_process(hub):
    PROFILES = {"provider.test": {"default": {}}}
    profiles = await hub.acct.init.process(subs=["provider"], profiles=PROFILES)
    assert profiles == {"provider": {"default": {"processed": True}}}


@pytest.mark.asyncio
async def test_unlock(hub):
    backends = await hub.acct.backend.init.unlock(profiles=PROFILES)
    assert backends == {
        "provider.test": {"profile1": {"kwarg1": "val1"}, "profile2": {}}
    }


@pytest.mark.asyncio
async def test_gather(hub):
    p1 = await hub.acct.init.gather(
        subs=["provider"], profile="profile1", profiles=PROFILES
    )
    assert p1 == {"kwarg1": "val1", "processed": True}

    p2 = await hub.acct.init.gather(
        subs=["provider"], profile="profile2", profiles=PROFILES
    )
    assert p2 == {"processed": True}

    d = await hub.acct.init.gather(
        subs=["provider"], profile="default", profiles=PROFILES
    )
    assert d == {"processed": True}


@pytest.mark.asyncio
async def test_gather_undefined(hub):
    ctx_acct = await hub.acct.init.gather(
        subs=["provider"], profile="undefined", profiles=PROFILES
    )
    assert ctx_acct == {}


@pytest.mark.asyncio
async def test_gather_no_backends(hub):
    ctx_acct = await hub.acct.init.gather(subs=[], profile="default", profiles=PROFILES)
    assert ctx_acct == {}


@pytest.mark.asyncio
async def test_gather_unlock(hub):
    """
    Verify that a normal gather function gets processed
    """
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {"provider": {"profile": {"kwarg1": "value1"}}}

    ctx_acct = await hub.acct.init.gather(subs=["provider"], profile="profile")
    assert ctx_acct == {"kwarg1": "value1", "processed": True}
