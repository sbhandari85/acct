import tempfile
from unittest import mock

import pop.contract
import pop.hub
import pytest
import yaml
from dict_tools.data import NamespaceDict

credentials_yaml = """
backend_key: my-backend-key

default: my-default

provider:
  profile:
    kwarg1: value1
    list1:
      - item1
      - item2
"""


def test_cli():
    """
    Test the encryption and decryption capabilities of the cli tool
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="acct")

    # A temporary file to write encoded data to and read encoded data from
    with tempfile.NamedTemporaryFile(delete=True) as enc_fh:
        # A temporary file to write unencrypted data to
        with tempfile.NamedTemporaryFile(delete=True) as raw_fh:
            raw_fh.write(credentials_yaml.encode())
            raw_fh.flush()
            with mock.patch(
                "sys.argv",
                [
                    "acct",
                    "encrypt",
                    raw_fh.name,
                    "--crypto-plugin=fernet",
                    f"--output-file={enc_fh.name}",
                ],
            ):
                with mock.patch("os.environ", {}):
                    with mock.patch("builtins.print") as printed:
                        with mock.patch("sys.exit"):
                            hub.acct.init.cli()
                        assert printed.call_args
                        # Retrieve the key that was printed out
                        acct_key = printed.call_args[0][0]

        # Decrypt a the encrypted file with the key
        with mock.patch(
            "sys.argv",
            [
                "acct",
                "decrypt",
                enc_fh.name,
                f"--acct-key={acct_key}",
                "--crypto-plugin=fernet",
            ],
        ):
            with mock.patch("builtins.print") as printed:
                with mock.patch("sys.exit"):
                    hub.acct.init.cli()

                # Retrieve the output of the decryption process
                decrypted_file = printed.call_args[0][0]

    # Compare notes, they should be exactly  the same
    assert yaml.safe_load(decrypted_file) == yaml.safe_load(credentials_yaml)


@pytest.mark.asyncio
async def test_profiles(hub):
    data = {"foo": "bar", "baz": "bop"}

    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = await hub.crypto.init.generate_key(plugin="fernet")
        enc = await hub.crypto.init.encrypt(data=data, key=key, plugin="fernet")
        fh.write(enc)
        fh.flush()

        profiles = await hub.acct.init.profiles(
            acct_file=fh.name, acct_key=key, crypto_plugin="fernet"
        )

    assert profiles == data


@pytest.mark.asyncio
async def test_profiles_plaintext(hub):
    data = {"foo": "bar", "baz": "bop"}

    with tempfile.NamedTemporaryFile(delete=True, mode="w") as fh:
        yaml.safe_dump(data, fh)
        fh.flush()

        profiles = await hub.acct.init.profiles(acct_file=fh.name)

    assert profiles == data


@pytest.mark.asyncio
async def test_unlock(hub):
    data = {
        "default": "my_default",
        "backend_key": "my_backend_key",
        "foo": {"bar": ["baz"]},
    }

    with tempfile.NamedTemporaryFile(delete=True) as fh:
        key = await hub.crypto.init.generate_key(plugin="fernet")
        enc = await hub.crypto.init.encrypt(data=data, key=key, plugin="fernet")
        fh.write(enc)
        fh.flush()

        await hub.acct.init.unlock(acct_file=fh.name, acct_key=key)
        assert hub.acct.UNLOCKED
        assert hub.acct.DEFAULT == "my_default"
        assert hub.acct.BACKEND_KEY == "my_backend_key"
        assert hub.acct.PROFILES == {"foo": {"bar": ["baz"]}}


@pytest.mark.asyncio
async def test_single(hub):
    ctx = {"kw1": "v1"}
    ret = await hub.acct.init.single(
        subs=["foo"],
        profile_name="bar",
        profiles={"foo": {"bar": ctx}},
        sub_profiles=...,
    )
    assert ret == ctx


@pytest.mark.asyncio
async def test_gather(hub):
    hub.acct.UNLOCKED = True
    await hub.acct.init.gather([], "")


@pytest.mark.asyncio
async def test_gather(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {"my_provider": {"my_profile": {"kwarg1": "value1"}}}

    ctx_acct = await hub.acct.init.gather(subs=["my_provider"], profile="my_profile")
    assert ctx_acct == {"kwarg1": "value1"}


@pytest.mark.asyncio
async def test_gather_no_profile_match(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {"my_provider": {"non_matching_profile": ...}}

    ctx_acct = await hub.acct.init.gather(subs=["my_provider"], profile="my_profile")
    assert ctx_acct == {}


@pytest.mark.asyncio
async def test_gather_no_matching_subs(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {"my_provider": {"my_profile": {"kwarg1": "value1"}}}

    ctx_acct = await hub.acct.init.gather(
        subs=["my_provider.foo"], profile="my_profile"
    )
    assert ctx_acct == {}


@pytest.mark.asyncio
async def test_gather_no_args(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {"my_provider": {"my_profile": {"kwarg1": "value1"}}}

    ctx_acct = await hub.acct.init.gather(subs=[], profile=None)
    assert ctx_acct == {}


@pytest.mark.asyncio
async def test_gather_multiple_def(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {
        "my_provider": {"my_profile": {"kwarg1": "value1"}},
        "my_provider2": {"my_profile": {}},
    }

    with pytest.raises(ValueError):
        await hub.acct.init.gather(
            subs=["my_provider", "my_provider2"], profile="my_profile"
        )


@pytest.mark.asyncio
async def test_gather_multiple_def_subset(hub):
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {
        "my_provider": {"my_profile": {"kwarg1": "value1"}},
        "my_provider2": {"my_profile": {}},
    }

    ctx_acct = await hub.acct.init.gather(subs=["my_provider"], profile="my_profile")
    assert ctx_acct == {"kwarg1": "value1"}


@pytest.mark.asyncio
async def test_unlock_blob(hub):
    data = {
        "default": "my_default",
        "backend_key": "my_backend_key",
        "foo": {"bar": ["baz"]},
    }

    key = await hub.crypto.init.generate_key(plugin="fernet")
    enc = await hub.crypto.init.encrypt(data=data, key=key, plugin="fernet")

    ret = await hub.acct.init.unlock_blob(enc, acct_key=key, crypto_plugin="fernet")

    assert ret == {
        "default_profile": "my_default",
        "backend_key": "my_backend_key",
        "sub_profiles": None,
        "profiles": {
            "foo": {"bar": ["baz"]},
            "backend_key": "my_backend_key",
            "default": "my_default",
        },
    }


@pytest.mark.asyncio
async def test_overrides(hub):
    """
    Test the ability of config to do an override
    """
    hub.OPT = NamespaceDict(
        acct=dict(overrides=dict(my_provider=dict(my_profile=dict(kwarg1="override"))))
    )
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {
        "my_provider": {"my_profile": {"kwarg1": "value1", "kwarg2": "value2"}},
    }

    ctx_acct = await hub.acct.init.gather(subs=["my_provider"], profile="my_profile")
    assert ctx_acct == {"kwarg1": "override", "kwarg2": "value2"}


@pytest.mark.asyncio
async def test_overrides_empty(hub):
    """
    Empty config should cause no problems
    """
    hub.OPT = {}
    hub.acct.UNLOCKED = True
    hub.acct.PROFILES = {
        "my_provider": {"my_profile": {"kwarg1": "value1", "kwarg2": "value2"}},
    }

    ctx_acct = await hub.acct.init.gather(subs=["my_provider"], profile="my_profile")
    assert ctx_acct == {"kwarg1": "value1", "kwarg2": "value2"}
