import pathlib
from unittest import mock

import pytest


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="acct")

    with mock.patch("sys.argv", ["acct"]):
        hub.pop.config.load(["acct"], cli="acct")

    yield hub


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.parent.absolute()


@pytest.fixture(scope="session")
def runpy(code_dir) -> str:
    return str(code_dir.joinpath("run.py"))
