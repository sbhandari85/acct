from typing import Any
from typing import Dict

from dict_tools.data import NamespaceDict


async def gather(hub, profile: Dict[str, Any]):
    """
    Get the metadata for the given profile.
    The profile contains the login information for a provider, similar to ctx.acct in idem states
    """
    account_id = hub.acct.metadata.provider.some_external_api(
        ctx=NamespaceDict(acct=profile)
    )
    return {
        "account_id": account_id,
    }


def some_external_api(hub, ctx):
    # Use ctx.acct to connect to an external api and get a unique id related to these credentials
    return hash(str(ctx.acct))
