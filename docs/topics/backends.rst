BACKENDS
========

You can make an acct backend plugin to collect profiles from an alternate source.
Backends are processed after the initial encrypted acct_file reading, but before profiles are processed.
This makes it so you can define credentials to access an external secret store in your `acct_file` --
and then define extra profiles within that external secret store.  acct backend plugins transform data from
that external secret store into acct profiles. The "ctx" parameter will receive profile information
for unlocking backends. The function can be synchronous or asynchronous.

my_project/acct/backend/my_plugin.py

.. code-block:: python

        async def unlock(hub, **ctx):
            """
            Get profiles from an external store

            Example:

            .. code-block:: yaml

                acct-backends:
                  my_backend_plugin:
                    arbitrary_profile_name:
                      username: XXXXXXXXXXXX
                      password: XXXXXXXXXXXX
                      api_key: XXXXXXXXXXXXXXXXXXXXXXX
            """
            # Create a connection through [some_library] for the profile defined in 'ctx'
            connection = some_library.connect(**ctx)

            # get profile information from the connection and turn it into a dictionary that looks like:
            # {"provider": "profile1":  {"kwarg1": "value1"}
            return await connection.get_profiles()
