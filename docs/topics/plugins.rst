PLUGINS
=======

Create the directory  `my_project/acct/my_project` and add your acct plugins there.
`acct` plugins need to implement a `gather` function.
If your app used "hub.acct.init.unlock()" to initialize profile data then you can read profiles from
`hub.acct.PROFILES` as a starting point for sub_profile data.
`gather()` should return a dictionary of processed profiles.
Gather functions can be asynchronous or synchronous.

my_project/acct/my_project/my_plugin.py

.. code-block:: python

    async def gather(hub):
        """
        Get [my_plugin] profiles from an encrypted file

        Example:

        .. code-block:: yaml

            my_plugin:
              profile_name:
                username: XXXXXXXXXXXX
                password: XXXXXXXXXXXX
                api_key: XXXXXXXXXXXXXXXXXXXXXXX
        """
        processed_profiles = {}
        for profile, ctx in hub.acct.PROFILES.get("my_plugin", {}).items():
            # Create a connection through [some_library] for each of the profiles
            sub_profiles[profile] = {
                "connected": False,
                "connection": await some_library.connect(**ctx),
            }
        return processed_profiles

The `gather` function can optionally take a "profiles" parameter.
"profiles" will be an implicitly passed dictionary of data that was read from the encrypted acct file.
This is useful when profiles are collected explicitly by your own program and aren't stored in the
traditional location within acct.

my_project/acct/my_project/my_plugin.py

.. code-block:: python

    async def gather(hub, profiles):
        """
        Get [my_plugin] profiles from an encrypted file

        Example:

        .. code-block:: yaml

            my_plugin:
              profile_name:
                username: XXXXXXXXXXXX
                password: XXXXXXXXXXXX
                api_key: XXXXXXXXXXXXXXXXXXXXXXX
        """
        processed_profiles = {}
        for profile, ctx in profiles.get("my_plugin", {}).items():
            # Create a connection through [some_library] for each of the profiles
            sub_profiles[profile] = {
                "connected": False,
                "connection": await some_library.connect(**ctx),
            }
        return processed_profiles
