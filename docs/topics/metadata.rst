METADATA
========

You can make an acct backend plugin to collect metadata about a profile.
Metadata functions are not called directly from anywhere within acct.

Metadata functions must be called from an external project on an as-needed basis like so:

.. code-block:: python

    async def my_func(hub):
        acct_metadata = await hub.acct.metadata.init.gather(
            profile={"username": "my_user", "password": "my_password"},
            providers=["my_provider"],
        )


Projects can implement a metadata plugin that collects information about an acct profile:

.. code-block:: python

    # my_project/acct/metadata/my_provider.py
    from typing import Any
    from typing import Dict
    from dict_tools.data import NamespaceDict


    async def gather(hub, profile: Dict[str, Any]):
        """
        Get the metadata for the given profile.
        :param profile: contains connection credentials for the "my_provider" provider
        """
        # Create a connection through [some_library] for the profile defined in 'profile'
        connection = some_library.connect(**profile)

        # get metadata using the profile from the connection
        return await connection.get_profile_metadata()
