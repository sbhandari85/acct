.. acct documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to acct's Documentation!
================================


.. toctree::
   :maxdepth: 2
   :glob:

   topics/cli
   topics/cli_merge
   topics/vertical_merge
   topics/plugins
   topics/backends
   topics/metadata
   topics/integration
   topics/contributing
   tutorial/index
   releases/index

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/saltstack/pop/acct/>

Indices and tables
==================

* :ref:`modindex`
